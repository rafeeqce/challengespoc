//
//  Player.swift
//  ChallengesPOC
//
//  Created by Rafeeq CE on 11/08/16.
//  Copyright © 2016 Mobiefit. All rights reserved.
//

import Foundation

struct Player {
    var firstname: String?
    var lastname: String?
    var fullname : String {
        switch (firstname, lastname) {
        case (.Some, .Some):
            return firstname! + " " + lastname!
            
        case (.None, .Some):
            return lastname!
            
        case (.Some, .None):
            return firstname!
            
        default:
            return ""
        }
    }
    
    var rank: String?
    var total_steps: Int?
    var contrib: Double?
}