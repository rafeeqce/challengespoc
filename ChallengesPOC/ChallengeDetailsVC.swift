//
//  ChallengeDetailsVC.swift
//  ChallengesPOC
//
//  Created by Rafeeq CE on 10/08/16.
//  Copyright © 2016 Mobiefit. All rights reserved.
//

import UIKit
import Alamofire
import QuartzCore

class ChallengeDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var challenge: Challenge?
    var currentUser: User?
    var currentGroup: Group?
    var groups = [Group]()
    
    @IBOutlet weak var lblChallenge: UILabel!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = challenge!.name
        lblChallenge.text = challenge!.name
        
        self.tableView.layer.borderWidth = 1.0
        self.tableView.layer.borderColor = UIColor.blueColor().CGColor
        
        fetchChallengeDetails({
            self.tableView.reloadData()
        })
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "GroupTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! GroupTableViewCell
        
        let team = groups[indexPath.row]
        
        cell.lblName.text = team.name
        cell.lblRank.text = "Rank " + team.rank!
        cell.lblDistance.text = team.total_steps! + " steps"
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        currentGroup = groups[indexPath.row]
        
        self.performSegueWithIdentifier("TeamDetails", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "TeamDetails" {
            
            //let nav = segue.destinationViewController as! UINavigationController
            
            guard let controller = segue.destinationViewController as? TeamDetailsVC else {
                fatalError("Invalid storyboard configuration. Controller is not expected type ChallengesController")
            }
            
            controller.challenge = challenge
            controller.currentUser = currentUser
            controller.group = currentGroup
        }
    }

    func fetchChallengeDetails(completion: ()->Void)
    {
        let parameters = ["challenge_id": challenge!.challenge_id!]
        
        let request = NSMutableURLRequest(URL: NSURL(string: "http://service-staging.mobiefit.com/challenge/single")!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("office", forHTTPHeaderField: "x-product-name")
        request.setValue(currentUser!.authToken, forHTTPHeaderField: "x-access-token")
        
        do {
            request.HTTPBody  = try NSJSONSerialization.dataWithJSONObject(parameters, options:[])
        } catch let error as NSError! {
            print("JSON serialization failed:  \(error)")
        }
        
        Alamofire.request(request)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                // 1.
                guard response.result.isSuccess else {
                    print("Error while uploading file: \(response.result.error)")
                    return
                }
                
                // 2.
                guard let responseJSON = response.result.value as? [String: AnyObject],
                    chlng = responseJSON["challenge"] as? [String: AnyObject],
                    groups = chlng["group_leaderboard"] as? [AnyObject]
                    else {
                        print("Invalid information received from service")
                        return
                }
                
                for grp in groups {
                    var team = Group()
                    team.group_id = grp["group_id"] as? Int
                    team.name = grp["name"] as? String
                    team.rank = grp["rank"] as? String
                    team.total_steps = grp["total_steps"] as? String
                    
                    self.groups.append(team)
                }
                
                completion()
        }
    }
}
