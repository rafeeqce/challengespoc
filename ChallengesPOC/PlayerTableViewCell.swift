//
//  PlayerTableViewCell.swift
//  ChallengesPOC
//
//  Created by Rafeeq CE on 11/08/16.
//  Copyright © 2016 Mobiefit. All rights reserved.
//

import UIKit

class PlayerTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    
    @IBOutlet weak var lblContrib: UILabel!
}
