//
//  Challenge.swift
//  ChallengesPOC
//
//  Created by Rafeeq CE on 10/08/16.
//  Copyright © 2016 Mobiefit. All rights reserved.
//

import Foundation

struct Challenge {
    var challenge_id: Int?
    var name: String?
}
