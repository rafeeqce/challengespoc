//
//  Team.swift
//  ChallengesPOC
//
//  Created by Rafeeq CE on 10/08/16.
//  Copyright © 2016 Mobiefit. All rights reserved.
//

import Foundation

struct Group {
    var group_id: Int?
    var name: String?
    var rank: String?
    var total_steps: String?
}