//
//  ViewController.swift
//  ChallengesPOC
//
//  Created by Rafeeq CE on 09/08/16.
//  Copyright © 2016 Mobiefit. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    
    var currentUser: User?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentUser = User()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        GIDSignIn.sharedInstance().signInSilently()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func signIn(signIn: GIDSignIn!, didSignInForUser guser: GIDGoogleUser!, withError error: NSError!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            currentUser!.email = guser.profile.email
            currentUser!.firstname = guser.profile.givenName
            currentUser!.lastname = guser.profile.familyName
            
            validateUser(currentUser!, completion: { [unowned self] authToken in
                
                self.performSegueWithIdentifier("Challenges", sender: nil)
            })
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Challenges" {
            
            //let nav = segue.destinationViewController as! UINavigationController
            
            guard let controller = segue.destinationViewController as? ChallengesListVC else {
                fatalError("Invalid storyboard configuration. Controller is not expected type ChallengesController")
            }
            
            controller.currentUser = currentUser
        }
    }
    
    func validateUser(user: User, completion: (String)->Void) {
        let parameters  = [
            "device_id": 1,
            "firstname": user.firstname!,
            "lastname": user.lastname!,
            "email": user.email!,
            "authData" : [
                "google"    : [
                    "email" : user.email!
                ]
            ]
        ]
        
        let request = NSMutableURLRequest(URL: NSURL(string:"http://service-staging.mobiefit.com/user/")!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("office", forHTTPHeaderField: "x-product-name")
        
        do {
            request.HTTPBody  = try NSJSONSerialization.dataWithJSONObject(parameters, options:[])
        } catch let error as NSError! {
            print("JSON serialization failed:  \(error)")
        }

        Alamofire.request(request)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                // 1.
                guard response.result.isSuccess else {
                    print("Error while uploading file: \(response.result.error)")
                    return
                }
                // 2.
                guard let responseJSON = response.result.value as? [String: AnyObject],
                    authToken = responseJSON["api_access_token"] as? String else {
                        print("Invalid information received from service")
                        return
                }
                
                self.currentUser!.authToken = authToken
                completion(authToken)
        }
        
    }
}

