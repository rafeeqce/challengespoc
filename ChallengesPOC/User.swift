//
//  User.swift
//  ChallengesPOC
//
//  Created by Rafeeq CE on 10/08/16.
//  Copyright © 2016 Mobiefit. All rights reserved.
//

import Foundation

struct User {
    var email: String?
    var firstname: String?
    var lastname: String?
    var authToken: String?
}