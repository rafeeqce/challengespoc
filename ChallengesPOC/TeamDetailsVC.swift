//
//  TeamDetailsVC.swift
//  ChallengesPOC
//
//  Created by Rafeeq CE on 11/08/16.
//  Copyright © 2016 Mobiefit. All rights reserved.
//

import UIKit
import Alamofire

class TeamDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var currentUser: User?
    var challenge: Challenge?
    var group: Group?
    
    
    @IBOutlet weak var lblChallenge: UILabel!
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var lblSteps: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var players = [Player]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblChallenge.text = challenge!.name
        lblTeam.text = group!.name
        lblSteps.text = group!.total_steps
        
        self.navigationItem.title = challenge!.name
        
        self.tableView.layer.borderWidth = 1.0
        self.tableView.layer.borderColor = UIColor.blueColor().CGColor
        
        fetchTeamDetails ({
            self.tableView.reloadData()
        })
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "PlayerTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PlayerTableViewCell
        
        let player = players[indexPath.row]
        
        cell.lblName.text = player.fullname
        cell.lblDistance.text = String(player.total_steps!) + " steps"
        cell.lblContrib.text = String(player.contrib!) + "%"
        
        return cell
    }
    
    func fetchTeamDetails(completion: ()->Void)
    {
        let parameters = ["challenge_id": challenge!.challenge_id!, "group_id": group!.group_id!]
        
        let request = NSMutableURLRequest(URL: NSURL(string: "http://service-staging.mobiefit.com/challenge/leaderboard/groupmember")!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("office", forHTTPHeaderField: "x-product-name")
        request.setValue(currentUser!.authToken, forHTTPHeaderField: "x-access-token")
        
        do {
            request.HTTPBody  = try NSJSONSerialization.dataWithJSONObject(parameters, options:[])
        } catch let error as NSError! {
            print("JSON serialization failed:  \(error)")
        }
        
        Alamofire.request(request)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                // 1.
                guard response.result.isSuccess else {
                    print("Error while uploading file: \(response.result.error)")
                    return
                }
                
                // 2.
                guard let responseJSON = response.result.value as? [String: AnyObject],
                    players = responseJSON["leaderboard"] as? [AnyObject]
                    else {
                        print("Invalid information received from service")
                        return
                }
                
                let total_count: Int? = Int(self.group!.total_steps!)
                
                for member in players {
                    var player = Player()
                    player.firstname = member["firstname"] as? String
                    player.lastname = member["lastname"] as? String
                    player.rank = member["rank"] as? String
                    player.total_steps = member["total_steps"] as? Int
                    
                    if total_count != 0 {
                        player.contrib = (Double)(player.total_steps! * 100 / total_count!)
                    }
                    else {
                        player.contrib = 0.0
                    }
                    self.players.append(player)
                }
                
                
                completion()
        }
    }

}
