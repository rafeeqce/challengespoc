//
//  ChallengesController.swift
//  ChallengesPOC
//
//  Created by Rafeeq CE on 10/08/16.
//  Copyright © 2016 Mobiefit. All rights reserved.
//

import UIKit
import Alamofire
import HealthKit

class ChallengesListVC: UITableViewController {
    var currentUser: User?
    var challenges = [Challenge]()
    var currentChallenge: Challenge?
    
    let healthManager = HealthKitManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        getHealthKitPermission()
        
        fetchChallenges({
            self.tableView.reloadData()
        })
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.hidesBackButton = true
        
        healthManager.recentSteps({steps, error in
            self.updateStepCount(steps)
        })
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return challenges.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")
        cell!.textLabel?.text = challenges[indexPath.row].name
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        currentChallenge = challenges[indexPath.row]
        
        self.performSegueWithIdentifier("ChallengeDetails", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ChallengeDetails" {
            
            //let nav = segue.destinationViewController as! UINavigationController
            
            guard let controller = segue.destinationViewController as? ChallengeDetailsVC else {
                fatalError("Invalid storyboard configuration. Controller is not expected type ChallengesController")
            }
            
            controller.challenge = currentChallenge
            controller.currentUser = currentUser
        }
    }
    
    func getHealthKitPermission() {
        
        // Seek authorization in HealthKitManager.swift.
        healthManager.authorizeHealthKit { (authorized,  error) -> Void in
            if authorized {
                
                // Get and set the user's height.
                //self.setHeight()
            } else {
                if error != nil {
                    print(error)
                }
                print("Permission denied.")
            }
        }
    }
    
    func updateStepCount(steps: Double?)
    {
        let parameters = ["total_steps": steps!]
        
        let request = NSMutableURLRequest(URL: NSURL(string: "http://service-staging.mobiefit.com/user/wearables/google/data")!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("office", forHTTPHeaderField: "x-product-name")
        request.setValue(currentUser!.authToken, forHTTPHeaderField: "x-access-token")
        
        do {
            request.HTTPBody  = try NSJSONSerialization.dataWithJSONObject(parameters, options:[])
        } catch let error as NSError! {
            print("JSON serialization failed:  \(error)")
        }
        
        Alamofire.request(request)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                // 1.
                guard response.result.isSuccess else {
                    print("Error while uploading file: \(response.result.error)")
                    return
                }
            }
        
        
    }
    
    func fetchChallenges(completion: ()->Void)
    {
        let parameters = ["timeline": "current"]
        
        let request = NSMutableURLRequest(URL: NSURL(string: "http://service-staging.mobiefit.com/challenge/list")!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("office", forHTTPHeaderField: "x-product-name")
        request.setValue(currentUser!.authToken, forHTTPHeaderField: "x-access-token")
        
        do {
            request.HTTPBody  = try NSJSONSerialization.dataWithJSONObject(parameters, options:[])
        } catch let error as NSError! {
            print("JSON serialization failed:  \(error)")
        }
        
        Alamofire.request(request)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                // 1.
                guard response.result.isSuccess else {
                    print("Error while uploading file: \(response.result.error)")
                    return
                }
                
                var challenge = Challenge()
                
                // 2.
                guard let responseJSON = response.result.value as? [String: AnyObject],
                    results = responseJSON["challenges"] as? [AnyObject],
                    firstResult = results.first as? [String: AnyObject],
                    name = firstResult["name"] as? String,
                    id = firstResult["uid"] as? Int
                    else {
                        print("Invalid information received from service")
                        return
                }
                
                challenge.challenge_id = id
                challenge.name = name
                
                self.challenges.append(challenge)
                
                completion()
        }
    }
}
